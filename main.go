package main

import (
	"bufio"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"time"
)

type IP struct {
	Query string
}

var (
	clients   = []*bufio.Writer{}
	localClip = ""
)

func main() {
	if len(os.Args) > 2 {
		fmt.Println("too many arguments")
		return
	}
	if len(os.Args) == 2 {
		connectServer(os.Args[1])
		return
	}
	runServer()
}

func runServer() {
	fmt.Println("Starting new clipboard server")
	ln, err := net.Listen("tcp", ":6969")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer ln.Close()

	address := getPublicIP()

	fmt.Println("Run `ohmyclip " + address + ":6969` to join clipboard")

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		go handlerClient(conn)
	}
}

func getPublicIP() string {
	req, err := http.Get("http://ip-api.com/json/")
	if err != nil {
		return err.Error()
	}
	defer req.Body.Close()

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return err.Error()
	}

	var ip IP
	json.Unmarshal(body, &ip)

	return ip.Query
}

func handlerClient(conn net.Conn) {
	defer conn.Close()
	fmt.Println("New device connected at " + conn.RemoteAddr().String())
	w := bufio.NewWriter(conn)
	clients = append(clients, w)
	fmt.Println(clients)
	go checkLocalClip(w)
	monitorSentClips(bufio.NewReader(conn))
}

func connectServer(address string) {
	conn, err := net.Dial("tcp4", address)
	if err != nil {
		fmt.Println(err)
		return
	}
	if conn == nil {
		fmt.Println("Cannot connect to ", address)
		return
	}
	defer conn.Close()
	fmt.Println("Connected successfully")
	go checkLocalClip(bufio.NewWriter(conn))
	monitorSentClips(bufio.NewReader(conn))
}

func getClip() string {
	var cmd *exec.Cmd
	switch runtime.GOOS {
	case "windows":
		cmd = exec.Command("powershell.exe", "-command", "Get-Clipboard")
	default:
		cmd = exec.Command("xclip", "-out", "-selection", "clipboard")
	}
	clip, err := cmd.Output()
	if err != nil {
		return err.Error()
	}
	return string(clip)
}

func checkLocalClip(w *bufio.Writer) {
	for {
		localClip = getClip()
		fmt.Println("Current clip: " + localClip)
		err := sendClip(w, localClip)
		if err != nil {
			fmt.Println(err)
			return
		}
		for localClip == getClip() {
			time.Sleep(time.Second * 1)
		}
	}
}

// func setClip(clip string) error {
// 	var cmd *exec.Cmd
// 	switch runtime.GOOS {
// 	case "windows":
// 		cmd = exec.Command("powershell.exe", "-command", "Get-Clipboard", clip)
// 	default:
// 		cli := "echo -n `" + clip + "` | xclip -in -selection clipboard"
// 		cmd = exec.Command("bash", "-c", cli)
// 	}

// 	cmd.Stderr = os.Stderr
// 	cmd.Stdout = os.Stdout

// 	return cmd.Run()
// }

func setClip(clip string) {
	var copyCmd *exec.Cmd
	switch runtime.GOOS {
	case "darwin":
		copyCmd = exec.Command("pbcopy")
	case "windows":
		copyCmd = exec.Command("powershell.exe", "-command", "Set-Clipboard") //-Value "+"\""+s+"\"")
	default:
		if _, err := exec.LookPath("xclip"); err == nil {
			copyCmd = exec.Command("xclip", "-in", "-selection", "clipboard")
		} else if _, err = exec.LookPath("xsel"); err == nil {
			copyCmd = exec.Command("xsel", "--input", "--clipboard")
		} else if _, err = exec.LookPath("wl-copy"); err == nil {
			copyCmd = exec.Command("wl-copy")
		} else if _, err = exec.LookPath("termux-clipboard-set"); err == nil {
			copyCmd = exec.Command("termux-clipboard-set")
		} else {
			os.Exit(2)
		}
	}
	in, err := copyCmd.StdinPipe()
	if err != nil {

		fmt.Println(err)
		return
	}
	if err = copyCmd.Start(); err != nil {

		fmt.Println(err)
		return
	}
	if runtime.GOOS != "windows" {
		if _, err = in.Write([]byte(clip)); err != nil {

			fmt.Println(err)
			return
		}
		if err = in.Close(); err != nil {

			fmt.Println(err)
			return
		}
	}
	if err = copyCmd.Wait(); err != nil {

		fmt.Println(err)
		return
	}
}

func monitorSentClips(r *bufio.Reader) {
	var foreignClip string
	var foreignClipBytes []byte

	for {
		err := gob.NewDecoder(r).Decode(&foreignClipBytes)
		if err != nil {
			fmt.Println(err)
			return
		}

		foreignClip = string(foreignClipBytes)
		if foreignClip == "" {
			continue
		}
		setClip(foreignClip)
		localClip = foreignClip
		for i := range clients {
			if clients[i] != nil {
				err := sendClip(clients[i], foreignClip)
				if err != nil {
					clients[i] = nil
				}
			}
		}
	}
}

func sendClip(w *bufio.Writer, clip string) error {
	var clipBytes []byte

	err := gob.NewEncoder(w).Encode(clipBytes)
	if err != nil {
		return err
	}

	return w.Flush()
}
